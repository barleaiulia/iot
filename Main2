package main;

import de.re.easymodbus.exceptions.ModbusException;
import de.re.easymodbus.modbusclient.ModbusClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

public class Main2 {

    //colors
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";

    public static void main(String[] args) throws ModbusException, IOException {

        
        //read from file
        String path = System.getProperty("user.dir");
        File modbusGw = new File(path + "\\resources\\modbus_gw.ini");
        File monitorHost = new File(path + "\\resources\\Monitor_Host_Publishers.conf");

        //input registers list
        ArrayList<InputRegister> inputRegisters = new ArrayList<>();
        ArrayList<Register> registers = new ArrayList<>();

        //connect to modbus
        ModbusClient modbusClient = new ModbusClient();
        modbusClient.Connect("127.0.0.1", 502);
        System.out.println("-----------------------------------------\n");

        //this.inputRegisters = modbusClient.ReadInputRegisters(0, 100);

        //read from modbusGw
        try {
            Scanner sc = new Scanner(modbusGw);

            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                if (line.startsWith("REGISTER = ")) {
                    //get rid of "REGISTER = "
                    line = line.substring(11, line.length());

                    String[] p = line.split(",");

                    inputRegisters.add(new InputRegister(p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8], p[9]));
                }
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //read from monitor host publishers
        try {
            Scanner sc = new Scanner(monitorHost);

            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                if (line.startsWith("[")) { //this means a new register is in file
                    String eui64;
                    Concentrator concentrator = new Concentrator();
                    List<Channel> channels = new ArrayList<>();

                    //
                    eui64 = line;
                    eui64 = eui64.replace(":", ""); //remove :
                    eui64 = eui64.substring(1, eui64.length() - 1); //remove [ ]

                    //read the whole register
                    String regLine = "[ ]";
                    while (sc.hasNextLine() && !regLine.equals("")) {
                        //read from next line
                        regLine = sc.nextLine();
                        //check if it's concentrator
                        if (regLine.startsWith("CONCENTRATOR = ")) {
                            String p[] = regLine.split(", ");
                            concentrator = new Concentrator(p[0], p[1], p[2], p[3], p[4], p[5], p[6]);
                        }
                        //check if it's a channel
                        if (regLine.startsWith("CHANNEL = ")) {
                            String p[] = regLine.split(", ");
                            channels.add(new Channel(p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8]));
                        }
                    }
                    //create the register object
                    registers.add(new Register(eui64, concentrator, channels));
                }
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //print input registers
        for (InputRegister inputRegister: inputRegisters) {
            System.out.println(inputRegister);
        }

        System.out.println("-----------------------------------------");

        //print registers
        for (Register register: registers) {
            System.out.println(register);
        }

        System.out.println("-----------------------------------------");

        //
        System.out.format("|%16s|%16s|%25s|%20s|\n", "Device EUI64","Input Reg. Value","Unit of measurement","Datetime of the read");
        System.out.format("+----------------+----------------+-------------------------+--------------------+\n");

        //we start to iterete through the input registers that are found in modbus_gw.ini
        for (int i = 0; i < inputRegisters.size(); i++) {

            //we take from the current input register: startaddr, wordcnt, eui64, status
            int startAddr = Integer.valueOf(inputRegisters.get(i).getStartAddr());
            int wordCnt = Integer.valueOf(inputRegisters.get(i).getWordCnt());
            String eui64 = inputRegisters.get(i).getEui64();
            String status = inputRegisters.get(i).getStatus();
            List<Channel> channels = null;

            //try to find the register from monitor_host_publishers that has the same eui64 that we saved above
            for (int k = 0; k < registers.size(); k++) {
                //if we find the right one we save its channels
                if (registers.get(k).getEui64().equals(eui64))
                    channels = registers.get(k).getChannels();
            }

            //read the input registers from modbus server from the start addr and with a quantity of wordcnt
            int[] mbInputRegisterValues = modbusClient.ReadInputRegisters(startAddr, wordCnt);

            //now we print the values
            for (int k = 0; k < wordCnt; k++) {
                //we verify if the input register from modbus_gw.ini has the last parameter (status) == 2.
                //if so, we verify if we are at our last value to read and if so, we know that we've got a status and we have to print status instead of register value
                    if (status.equals("2") && k == wordCnt - 1) {
                        if (mbInputRegisterValues[k] == 128)
                            //https://stackoverflow.com/questions/5762491/how-to-print-color-in-console-using-system-out-println
                            System.out.format("|%89s|\n", ANSI_GREEN + "STATUS: FRESH" + ANSI_RESET);
                        if (mbInputRegisterValues[k] == 20)
                            System.out.format("|%89s|\n", ANSI_YELLOW + "STATUS: STALE" + ANSI_RESET);
                        if (mbInputRegisterValues[k] == 0)
                            System.out.format("|%89s|\n", ANSI_RED + "STATUS: MISSING DATA OR CONNECTION" + ANSI_RESET);
                        System.out.format("+----------------+----------------+-------------------------+--------------------+\n");
                    }
                    //if the last parameter of the input register is != 2, we simply print the sensor value
                    else {
                        System.out.format("|%16s|%16s|%25s|%20s|\n", eui64, mbInputRegisterValues[k], channels.get(k).getUnitOfMeasurement(), Calendar.getInstance().getTime().toString().substring(0, 19));
                        System.out.format("+----------------+----------------+-------------------------+--------------------+\n");
                    }
            }
        }
    }
}